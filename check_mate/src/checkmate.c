/**
 * checkmate.c
 *
 * This program demonstrates how to use the header file "checkmate.h"
 * to check if a current situation on the chess board is check-mate.
 */
#include <unistd.h>
#include "checkmate.h"

/*
 * Main program
 */
int main (int argc, char * argv[]) {
    if (argc < 2) {
        return (1 - write(1, "\n", 1)); // 1 : stdout
    }

    int debug = 0;

    ////
    // 1. Create an instance of CM_Checker
    //
    CM_Checker checker;

    ////
    // 2. Initialize CM_Checker
    //
    CM_Initialize(&checker);

    ////
    // 3. Read chess board's rows
    //
    int i = 1;
    while (argv[i]) { // *arv != 0
        if (argv[i][0] == '-' && argv[i][1] == 'd') { // "-d"
            debug = 1;
        }
        else {
            CM_Read_Row(&checker, argv[i], i-1);
        }
        ++i;
    }

    ////
    // 4. Validate if a check-mate situation does exist
    //
    if (CM_Checkmate(&checker) == CM_RESULT_SUCCESS)
        write(1, "Success", 7);
    else
        write(1, "Fail", 4);

    //// #begin DEBUG
    if (debug) {
        printf("\n");
        CM_Print_Board(&checker);
    }
    //// #end DEBUG

    ////
    // 5. Release allocated memory
    //
    CM_Release(&checker);

    return 0;
}