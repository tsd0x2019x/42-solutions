/*
 * LinkedList.h
 * 
 * This header file provides an simple implementation of linked-list and its helper functions.
 * 
 */
#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#define LinkedList_API extern

//=======================================================================================================================

typedef struct node_t Node; // forward-declaration
struct node_t {
    char name; // name of chess piece
    unsigned int row;
    unsigned int col;
    Node * next;
};

typedef struct {
    unsigned int size;
    Node * begin;
    Node * end;
} List;


//=======================================================================================================================

LinkedList_API unsigned int LinkedList_AddNodeWithNameAndCoordinate (List * list, char name, unsigned int row, unsigned int col);
LinkedList_API unsigned int LinkedList_AddNode (List * list, Node * new_node);
LinkedList_API List * LinkedList_CreateList ();
LinkedList_API void LinkedList_ClearList (List * list);
LinkedList_API Node * LinkedList_GetNodeWithIndex (List * list, int index);
LinkedList_API Node * LinkedList_GetNodeWithCoordinate (List * list, unsigned int x, unsigned int y, int * dst_index);
LinkedList_API unsigned int LinkedList_RemoveNodeWithCoordinate (List * list, unsigned int x, unsigned int y);
LinkedList_API unsigned int LinkedList_RemoveNodeByReference (List * list, Node * node_to_remove);
LinkedList_API void LinkedList_PrintList (List * list);

//=======================================================================================================================

/*
 * LinkedList_AddNodeWithNameAndCoordinate
 *
 * Add new node with name and coordinate (row,col) at list end. Return 0 if no list exists (NULL). Otherwise, return (updated) list size.
 */
LinkedList_API unsigned int LinkedList_AddNodeWithNameAndCoordinate (List * list, char name, unsigned int row, unsigned int col) {
    if (list == NULL) return 0; // There is no list => Nothing to add.
    Node * new_node = (Node *) malloc(sizeof(Node));
    new_node->name = name;
    new_node->row = row;
    new_node->col = col;
    return LinkedList_AddNode(list, new_node);
}

/*
 * LinkedList_AddNode
 *
 * Add new node at the list end. Return 0 if list is NULL, i.e. no list exists. Otherwise, return (updated) list size.
 */
LinkedList_API unsigned int LinkedList_AddNode (List * list, Node * new_node) {
    if (list == NULL) return 0; // There is no list => Nothing to add.
    if (list->size == 0) { // Empty list.
        list->begin = list->end = new_node;
    }
    else {
        list->end->next = new_node;
        list->end = new_node;
    }
    list->end->next = NULL;
    return ++(list->size);
}

/*
 * LinkedList_CreateList
 * 
 * Allocate memory to create a new (empty) list object and return a pointer to it.
 */
LinkedList_API List * LinkedList_CreateList () {
    List * list = (List *) malloc(sizeof(List));
    list->begin = list->end = NULL;
    list->size = 0;
    return list;
}

/*
 * LinkedList_ClearList
 * 
 * Free all allocated memory for the list object and its nodes.
 */
LinkedList_API void LinkedList_ClearList (List * list) {
    if (list) { // list != 0 = NULL
        if (list->size > 0) {
            Node * next = NULL;
            do {
                next = list->begin->next;
                list->begin->next = NULL;
                free(list->begin);
                --(list->size);
                list->begin = next;
            }
            while (next);
            list->end = NULL;
        }
        free(list);
        list = NULL;
    }
}

/*
 * LinkedList_GetNodeWithIndex
 * 
 * Provides a reference of the node at index [index] in the list. If the list does not exist (is NULL) or empty
 * (size = 0), a NULL pointer is returned. If index is smaller than zero (0) or bigger equal to list size, NULL
 * pointer is returned as well.
 */
LinkedList_API Node * LinkedList_GetNodeWithIndex (List * list, int index) {
    if (list == NULL || list->size == 0 || index < 0 || index >= list->size) {
        return NULL;
    }
    Node * node = list->begin;
    while (index > 0) {
        index--;
        node = node->next;
    }
    return node;
}

/*
 * LinkedList_GetNodeWithCoordinate
 * 
 * Provides a reference of the node that contains coordinate (row,col). If the list does not exist (is NULL) or empty
 * (size = 0), a NULL pointer is returned. The parameter [dst_index] is a variable containing the list index of
 * that node with coordinate (row,col). If node NOT found, [dst_index] == list->size. Otherwise, if found, always
 * 0 <= [dst_index] < list->size.
 */
LinkedList_API Node * LinkedList_GetNodeWithCoordinate (List * list, unsigned int row, unsigned int col, int * dst_index) {
    if (list == NULL || list->size == 0) {
        if (dst_index != NULL) *dst_index = -1;
        return NULL;
    }
    Node * result = NULL;
    Node * node = list->begin;
    int i = 0;
    do {
        if (node->row == row && node->col == col) {
            result = node;
            break;
        }
        ++i;
    } while ((node=node->next));
    if (dst_index != NULL) *dst_index = i; // if found, then always (i < list->size)
    return result;
}

/*
 * LinkedList_RemoveNodeWithCoordinate
 * 
 * Remove the (first) node with coordinate (row,col). Return 0 if no list eixsts or list is being empty.
 * Otherwise, return (updated) list size.
 */
LinkedList_API unsigned int LinkedList_RemoveNodeWithCoordinate (List * list, unsigned int row, unsigned int col) {
    if (list == NULL || list->size == 0) return 0;
    if (list->size == 1) {
        if (list->begin->row == row && list->begin->col == col) {
            free(list->begin);
            list->begin = list->end = NULL;
            --(list->size);
        }
    }
    else { // list->size > 1
        int index = 0;
        Node * node = LinkedList_GetNodeWithCoordinate(list, row, col, &index); // Current node to be removed
        if (index > -1 && index < list->size) { // Node with coordinate (row,col) does exist in list
            if (index == 0) { // First node
                Node * tmp = node->next; // = list->begin->next;
                node->next = NULL; // list->begin->next = NULL;
                list->begin = tmp;
            }
            else { // index > 0
                Node * prev = LinkedList_GetNodeWithIndex(list, index-1); // Previous node
                if (index == list->size - 1) { // Last node
                    list->end = prev;
                }                
                prev->next = node->next;
                node->next = NULL;
            }
            free(node);
            node = NULL;
            --(list->size);
        }
    }
    return list->size;
}

/*
 * LinkedList_RemoveNodeByReference
 *
 * Remove the node referred by the pointer <node_to_remove> from list. Attention: The pointer <node_to_remove>
 * must really point to a node in the list. Return the updated list size. If the node <node_to_remove>
 * is not in the list, nothing is removed and the list-size does not change.
 */
LinkedList_API unsigned int LinkedList_RemoveNodeByReference (List * list, Node * node_to_remove) {
    if (list == NULL || list->size == 0) return 0;
    if (list->size == 1) {
        if (list->begin == node_to_remove) {
            free(list->begin);
            list->begin = list->end = node_to_remove = NULL;
            --(list->size);
        }
    }
    else if (list->size > 1) {
        if (list->begin == node_to_remove) {
            list->begin = node_to_remove->next;
            node_to_remove->next = NULL;
            free(node_to_remove);
            node_to_remove = NULL;
            --(list->size);
        }
        else {
            Node * prev = list->begin; // The "previous" node that is in left of <node_to_remove>
            do {
                if (prev->next == node_to_remove) {
                    prev->next = node_to_remove->next;
                    node_to_remove->next = NULL;
                    if (node_to_remove == list->end) {
                        list->end = prev;
                    }
                    free(node_to_remove);
                    node_to_remove = NULL;
                    --(list->size);
                    break;
                }
            } while ((prev = prev->next) != NULL);
        }
    }
    return list->size;
}

/*
 * LinkedList_PrintList
 *
 * Display (all nodes of) the list.
 */
LinkedList_API void LinkedList_PrintList (List * list) {
    if (list == NULL) printf("NULL\n");
    else if (list->size == 0) printf("Empty: ()"); // Empty list
    else {
        int i = 0;
        Node * node = list->begin;
        printf("(%c,%u,%u)", node->name, node->row, node->col);
        while (i < (list->size-1)) {
            ++i;
            node = node->next;
            printf("->(%c,%u,%u)", node->name, node->row, node->col);
        }
        printf("\n");
    }
}

#ifdef __cplusplus
}
#endif

#endif /* LINKEDLIST_H */