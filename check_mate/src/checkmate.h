/**
 * checkmate.h
 * 
 * Description: For this exercice, one will only play with Pawns, Bishops, Rooks and Queen
 *              and obviously a King. The board have a variable size but will remains a square.
 *              There's only one King and all other pieces are against it. All other characters
 *              except those used for pieces are considered as empty squares. The King is
 *              considered as in a check position when an other enemy piece can capture it.
 * 
 */
#ifndef CHECKMATE_H
#define CHECKMATE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "LinkedList.h"
#define CM_API extern

//=======================================================================================================================

typedef enum {
    CM_RESULT_FAIL = 0,
    CM_RESULT_SUCCESS = 1
} CM_RESULT; // (Boolean) Return value

typedef struct {
    CM_RESULT result; // result whether check-mate is successfull or not
    int king_row; // row index of KING
    int king_col; // column index of KING
    unsigned int size; // size of square borad
    char ** board;
    List * list; // list contains the name and positions (x,y) of all pieces other than KING
} CM_Checker; /* Object for checking check-mate */

//=======================================================================================================================

CM_API void CM_Print_Board (CM_Checker * checker);
CM_API void CM_Release (CM_Checker * checker);
CM_API CM_RESULT CM_Verify_Rook_Checkmate (CM_Checker * checker, int piece_row, int piece_col);
CM_API CM_RESULT CM_Verify_Bishop_Checkmate (CM_Checker * checker, int piece_row, int piece_col);
CM_API CM_RESULT CM_Verify_Checkmate (CM_Checker * checker, char piece_name, int piece_row, int piece_col);
CM_API void CM_Init_Chessboard (CM_Checker * checker);
CM_API CM_RESULT CM_Checkmate (CM_Checker * checker);
CM_API void CM_Read_Row (CM_Checker * checker, char * row, int row_index);
CM_API void CM_Initialize (CM_Checker * checker);

/*
 * CM_Print_Board
 */
CM_API void CM_Print_Board (CM_Checker * checker) {
    for (int i = 0; i < checker->size; i++) {
        for (int j = 0; j < checker->size; j++) {
            if (checker->board[i][j] == 0)
                printf(" 0");
            else
                printf(" %c", checker->board[i][j]);
        }
        printf("\n");
    }
}

/*
 * CM_Release
 */
CM_API void CM_Release (CM_Checker * checker) {
    if (checker->list) {
        LinkedList_ClearList(checker->list);
        checker->list = NULL;
    }
    if (checker->board) {
        for (int i = 0; i < checker->size; ++i) {
            free(checker->board[i]);
            checker->board[i] = 0; // NULL
        }
        free(checker->board);
        checker->board = 0; // NULL
    }
}

/*
 * CM_Verify_Rook_Checkmate (OLD WORKING VERSION)
 *
 * Check if the rook (Turm) - on its movements - can check-mate the king.
 * 
 * @return
 *      CM_RESULT_SUCCESS : if there exists a check-mate situation
 *      CM_RESULT_FAIL    : otherwise
 */
/*
CM_API CM_RESULT CM_Verify_Rook_Checkmate (CM_Checker * checker, int piece_row, int piece_col) {
    if (piece_row < 0 || piece_row >= checker->size || piece_col < 0 || piece_col >= checker->size) {
        return CM_RESULT_FAIL;
    }
    CM_RESULT Result = CM_RESULT_FAIL;
    int row_down = piece_row, row_up = piece_row;
    int col_left = piece_col, col_right = piece_col;
    int i = 1;
    //// SAME ROW
    while (1) {
        if ((col_left-i) >= 0) { // LEFT
            if (checker->board[piece_row][col_left-i]) { // if NOT EMPTY SQUARE
                if (checker->board[piece_row][col_left-i] == 'K') { // King
                    Result = CM_RESULT_SUCCESS;
                    break;
                }
                else { // the square is not King, but also NOT EMPTY ==> Rook don't need to keep going on this direction
                    col_left = -1; // Condition to break the if-instruction
                }
            }
        }
        if ((col_right+i) < checker->size) { // RIGHT
            if (checker->board[piece_row][col_right+i]) { // if NOT EMPTY SQUARE
                if (checker->board[piece_row][col_right+i] == 'K') { // King
                    Result = CM_RESULT_SUCCESS;
                    break;
                }
                else { // the square is not King, but also NOT EMPTY ==> Rook don't need to keep going on this direction
                    col_right = checker->size; // Condition to break the if-instruction
                }
            }
        }
        if ((row_up-i) >= 0) { // UP
            if (checker->board[row_up-i][piece_col]) { // if NOT EMPTY SQUARE
                if (checker->board[row_up-i][piece_col] == 'K') { // King
                    Result = CM_RESULT_SUCCESS;
                    break;
                }
                else { // the square is not King, but also NOT EMPTY ==> Rook don't need to keep going on this direction
                    row_up = -1; // Condition to break the if-instruction
                }
            }
        }
        if ((row_down+i) < checker->size) { // DOWN
            if (checker->board[row_down+i][piece_col]) { // if NOT EMPTY SQUARE
                if (checker->board[row_down+i][piece_col] == 'K') { // King
                    Result = CM_RESULT_SUCCESS;
                    break;
                }
            }
            else { // the square is not King, but also NOT EMPTY ==> Rook don't need to keep going on this direction
                row_down = checker->size; // Condition to break the if-instruction
            }
        }
        if ((col_left-i) < 0 && (col_right+i) >= checker->size && (row_up-i) < 0 && (row_down+i) >= checker->size)
            break;
        ++i;
    }
    return Result;
}
*/

/*
 * CM_Verify_Rook_Checkmate (OLD WORKING IMPLEMENTATION)
 * 
 * Check if the rook (Turm) - on its movements - can check-mate the king.
 * 
 * @return
 *      CM_RESULT_SUCCESS : if there exists a check-mate situation
 *      CM_RESULT_FAIL    : otherwise
 */
CM_API CM_RESULT CM_Verify_Rook_Checkmate (CM_Checker * checker, int piece_row, int piece_col) {
    if (piece_row < 0 || piece_row >= checker->size || piece_col < 0 || piece_col >= checker->size) {
        return CM_RESULT_FAIL;
    }
    CM_RESULT Result = CM_RESULT_FAIL;
    int row = piece_row;
    int col = piece_col - 1;
    while (col >= 0) { // LEFT
        if (checker->board[row][col] == 'K') { // King
            Result = CM_RESULT_SUCCESS;
            break;
        }
        else { // checker->board[row][col] != KING
            if (checker->board[row][col]) { // checker->board[row][col] != 0
                break;
            }
        }
        --col;
    }
    if (Result == CM_RESULT_FAIL) {
        row = piece_row - 1;
        col = piece_col;
        while (row >= 0) { // UP
            if (checker->board[row][col] == 'K') { // King
                Result = CM_RESULT_SUCCESS;
                break;
            }
            else { // checker->board[row][col] != KING
                if (checker->board[row][col]) { // checker->board[row][col] != 0
                    break;
                }
            }
            --row;
        }
        if (Result == CM_RESULT_FAIL) {
            row = piece_row + 1;
            col = piece_col + 1;
            while (row < checker->size) { // DOWN
                if (checker->board[row][col] == 'K') { // King
                    Result = CM_RESULT_SUCCESS;
                    break;
                }
                else { // checker->board[row][col] != KING
                    if (checker->board[row][col]) { // checker->board[row][col] != 0
                        break;
                    }
                }
                ++row;
            }
            if (Result == CM_RESULT_FAIL) {
                row = piece_row;
                col = piece_col + 1;
                while (col < checker->size) { // RIGHT
                    if (checker->board[row][col] == 'K') { // King
                        Result = CM_RESULT_SUCCESS;
                        break;
                    }
                    else { // checker->board[row][col] != KING
                        if (checker->board[row][col]) { // checker->board[row][col] != 0
                            break;
                        }
                    }
                    ++col;
                }
            }
        }
    }
    return Result;
}

/*
 * CM_Verify_Bishop_Checkmate
 *
 * Check if the bishop - on its movements - can check-mate the king.
 * 
 * @return
 *      CM_RESULT_SUCCESS : if there exists a check-mate situation
 *      CM_RESULT_FAIL    : otherwise
 */
CM_API CM_RESULT CM_Verify_Bishop_Checkmate (CM_Checker * checker, int piece_row, int piece_col) {
    CM_RESULT Result = CM_RESULT_FAIL;
    int row = piece_row;
    int col = piece_col;
    int i = 1;
    //// UP LEFT + DOWN RIGHT
    while (1) {
        if ((row-i) >= 0 && (col-i) >= 0) { // UP LEFT
            if (checker->board[row-i][col-i] == 'K') { // King
                Result = CM_RESULT_SUCCESS;
                break;
            }
        }
        if ((row+i) < checker->size && (col+i) < checker->size) { // DOWN RIGHT
            if (checker->board[row+i][col+i] == 'K') { // King
                Result = CM_RESULT_SUCCESS;
                break;
            }
        }
        if ((row-i) < 0 && (col-i) < 0 && (row+i) >= checker->size && (col+i) >= checker->size)
            break;
        ++i;
    }
    //// Check intermediate result
    if (Result == CM_RESULT_SUCCESS)
        return Result;
    //// UP RIGHT + DOWN LEFT
    i = 1;
    while (1) {
        if ((row-i) >= 0 && (col+i) < checker->size) { // UP RIGHT
            if (checker->board[row-i][col+i] == 'K') { // King
                Result = CM_RESULT_SUCCESS;
                break;
            }
        }
        if ((row+i) < checker->size && (col-i) >= 0) { // DOWN LEFT
            if (checker->board[row+i][col-i] == 'K') { // King
                Result = CM_RESULT_SUCCESS;
                break;
            }
        }
        if ((row-i) < 0 && (col+i) >= checker->size && (row+i) >= checker->size && (col-i) < 0)
            break;
        ++i;
    }
    return Result;
}

/*
 * CM_Verify_Bishop_Checkmate (OLD WORKING IMPLEMENTATION)
 *
 * Check if the bishop - on its movements - can check-mate the king.
 * 
 * @return
 *      CM_RESULT_SUCCESS : if there exists a check-mate situation
 *      CM_RESULT_FAIL    : otherwise
 */
/*
CM_API CM_RESULT CM_Verify_Bishop_Checkmate (CM_Checker * checker, int piece_row, int piece_col) {
    CM_RESULT Result = CM_RESULT_FAIL;
    int row = piece_row - 1;
    int col = piece_col - 1;
    while (row >= 0 && col >= 0) { // LEFT UP
        if (checker->board[row][col] == 'K') { // King
            Result = CM_RESULT_SUCCESS;
            break;
        }
        else { // checker->board[row][col] != KING
            if (checker->board[row][col]) { // checker->board[row][col] != 0
                break;
            }
        }
        --row; --col;
    }
    if (Result == CM_RESULT_FAIL) {
        row = piece_row + 1;
        col = piece_col - 1;
        while (row < checker->size && col >= 0) { // LEFT DOWN
            if (checker->board[row][col] == 'K') { // King
                Result = CM_RESULT_SUCCESS;
                break;
            }
            else { // checker->board[row][col] != KING
                if (checker->board[row][col]) { // checker->board[row][col] != 0
                    break;
                }
            }
            ++row; --col;
        }
        if (Result == CM_RESULT_FAIL) {
            row = piece_row - 1;
            col = piece_col + 1;
            while (row >= 0 && col < checker->size) { // RIGHT UP
                if (checker->board[row][col] == 'K') { // King
                    Result = CM_RESULT_SUCCESS;
                    break;
                }
                else { // checker->board[row][col] != KING
                    if (checker->board[row][col]) { // checker->board[row][col] != 0
                        break;
                    }
                }
                --row; ++col;
            }
            if (Result == CM_RESULT_FAIL) {
                row = piece_row + 1;
                col = piece_col + 1;
                while (row < checker->size && col < checker->size) { // RIGHT DOWN
                    if (checker->board[row][col] == 'K') { // King
                        Result = CM_RESULT_SUCCESS;
                        break;
                    }
                    else { // checker->board[row][col] != KING
                        if (checker->board[row][col]) { // checker->board[row][col] != 0
                            break;
                        }
                    }
                    ++row; ++col;
                }
            }
        }
    }
    return Result;
}
*/

/*
 * CM_Verify_Checkmate
 * 
 * Check if the current piece can checkmate and kill the King in its next movement.
 *
 */
CM_API CM_RESULT CM_Verify_Checkmate (CM_Checker * checker, char piece_name, int piece_row, int piece_col) {
    CM_RESULT Result = CM_RESULT_FAIL;
    switch (piece_name) {
        case 'B': // Bishop =================================================================================
            Result = CM_Verify_Bishop_Checkmate(checker, piece_row, piece_col);
            break;
        case 'Q': // Queen ==================================================================================
            Result = CM_Verify_Bishop_Checkmate(checker, piece_row, piece_col);
            if (Result == CM_RESULT_FAIL)
                Result = CM_Verify_Rook_Checkmate(checker, piece_row, piece_col);
            break;
        case 'P': // Pawn ===================================================================================
            if ((piece_row-1) == checker->king_row && ((piece_col-1) == checker->king_col || (piece_col+1) == checker->king_col))
                Result = CM_RESULT_SUCCESS;
            break;
        case 'R': // Rook ===================================================================================
            Result = CM_Verify_Rook_Checkmate(checker, piece_row, piece_col);
            break;
        default: // empty square
            break;
    }
    return Result;
}

/*
 * CM_Init_Chessboard
 * 
 * Initialize the chess board, its check-mate checker and set the initial position of King.
 */
CM_API void CM_Init_Chessboard (CM_Checker * checker) {
    // Allocate memory for chess board
    checker->board = (char **) malloc(sizeof(char *) * checker->size);
    for (int i = 0; i < checker->size; ++i)
        checker->board[i] = (char *) malloc(sizeof(char) * checker->size);
    // Initialize with zeros
    for (int i = 0; i < checker->size; ++i)
        for (int j = 0; j < checker->size; ++j)
            checker->board[i][j] = 0; // EMPTY SQUARE (0)
    // Set positions (row,col) from checker->list
    if (checker->list->size > 0) {
        Node * node = checker->list->begin;
        do {
            checker->board[node->row][node->col] = node->name;
        }
        while ((node=node->next));
    }
    // Set King's position
    checker->board[checker->king_row][checker->king_col] = 'K';
}

/*
 * CM_Checkmate
 */
CM_API CM_RESULT CM_Checkmate (CM_Checker * checker) {
    // Create and initialize chess board with positions (row,col) from checker->list
    CM_Init_Chessboard(checker);
    // Check bad cases
    if (checker->king_row == -1 || checker->king_col == -1 || checker->list->size == 0) {
        return CM_RESULT_FAIL;
    }
    // Verify check-mate situation(s)
    CM_RESULT Result = CM_RESULT_FAIL;
    Node * node = checker->list->begin;
    do {
        if (CM_Verify_Checkmate(checker, node->name, node->row, node->col)) {
            Result = CM_RESULT_SUCCESS;
            break;
        }
    }
    while ((node=node->next));
    return Result;
}

/*
 * CM_Read_Row
 */
CM_API void CM_Read_Row (CM_Checker * checker, char * row, int row_index) {
    int i = 0; // column index
    while (row[i]) { // *ch != 0 (NULL)
        switch (row[i]) {
            case 'B': // Bishop
                LinkedList_AddNodeWithNameAndCoordinate(checker->list, 'B', row_index, i);
                break;
            case 'K': // King
                checker->king_row = row_index;
                checker->king_col = i;
                break;
            case 'Q': // Queen
                LinkedList_AddNodeWithNameAndCoordinate(checker->list, 'Q', row_index, i);
                break;
            case 'P': // Pawn
                LinkedList_AddNodeWithNameAndCoordinate(checker->list, 'P', row_index, i);
                break;
            case 'R': // Rook
                LinkedList_AddNodeWithNameAndCoordinate(checker->list, 'R', row_index, i);
                break;
            default: // empty square
                break;
        }
        ++i;
    }
    if (i > checker->size) { // size is maximum length of rows
        checker->size = i;
    }
}

/*
 * CM_Initialize
 */
CM_API void CM_Initialize (CM_Checker * checker) {
    checker->result = CM_RESULT_FAIL;
    checker->king_row = checker->king_col = -1;
    checker->size = 0;
    checker->board = 0;
    checker->list = LinkedList_CreateList(); // Create new linked-list
}

#ifdef __cplusplus
}
#endif

#endif /* CHECKMATE_H */
