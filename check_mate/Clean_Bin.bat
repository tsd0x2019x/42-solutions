@ECHO OFF
@REM =========================================
@REM    Clean_Bin.bat
@REM ==========================================
@SET MAIN_DIR=%CD%
@SET OUT_DIR=bin
@REM Remove all existing files in folder "bin\".
@REM /F forces to remove all protected files,
@REM /S to remove all files in all subfolders and
@REM /Q to disable the query/prompt if using place holder like "*" etc.
@DEL /F /Q /S %OUT_DIR%\*
@RMDIR %OUT_DIR%