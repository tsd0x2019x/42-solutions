#!/bin/bash

MAIN_DIR=$(pwd)
EXE=checkmate
ARGS=".. .K"

make clean && make

cd ./bin/  && ./${EXE} ${ARGS} -d

cd ${MAIN_DIR}
