@ECHO OFF
@REM ===================================================================================
@REM    make.bat
@REM ====================================================================================
@SET FileNameWithNoExtension=checkmate
@SET MAIN_DIR=%CD%
@SET SRC_DIR=src
@SET OUT_DIR=bin
@SET EXE=%FileNameWithNoExtension%64
@SET SRC_C=%FileNameWithNoExtension%.c
@SET SRC_H=%FileNameWithNoExtension%.h
@SET ARGS=.. .K
@REM ====================================================================================
@REM "x86_64-w64-mingw32-gcc.exe" is the equivalentof "gcc.exe" in Windows.
@SET MINGW64=x86_64-w64-mingw32-gcc.exe
@SET CC=%MINGW64%
@SET OPTIONS=-Wall -Werror -std=gnu99 -pedantic
@SET LINKOPT=-lm
@REM ====================================================================================
@REM Create folder "bin\"
@MKDIR %OUT_DIR%
@REM Delete all .exe files and subfolders in "bin/".
@REM Parameter /F forces to remove all protected files,
@REM /S to remove all files in all subfolders and
@REM /Q to disable the query/prompt if using place holder
@REM like "*" etc.
@DEL /F /S /Q %OUT_DIR%\%EXE%.exe
@REM Go back to main directory
@CD %MAIN_DIR%
@REM Compile for 64 bit
@%CC% %OPTIONS% %SRC_DIR%\%SRC_C% -o %OUT_DIR%\%EXE%.exe %LINKOPT%
@ECHO Build 64 bit binray checkmate64. Done.
@REM
@REM TODO:
@REM Configure MinGW and GCC to compile 32 bit
@REM binary on a 64 bit system.
@ECHO Start program %EXE%
@ECHO Command: %OUT_DIR%\%EXE%.exe %ARGS% -d
@CD %OUT_DIR%\
@%EXE%.exe %ARGS% -d
@CD %MAIN_DIR%
@PAUSE