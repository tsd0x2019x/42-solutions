# Checkmate

More information:

42-master: https://bitbucket.org/tsd0x2019x/42-master/src/master/

check_mate: https://bitbucket.org/tsd0x2019x/42-master/src/master/check_mate/subject.en.txt

            https://bitbucket.org/tsd0x2019x/42-master/src/master/check_mate/examples.txt

Assignment name  : check_mate

Expected files   : *.c, *.h

Allowed functions: write, malloc, free

--------------------------------------------------------------------------------

Write a program who takes rows of a chessboard in argument and check if your 
King is in a check position.

Chess is played on a chessboard, a squared board of 8-squares length with 
specific pieces on it : King, Queen, Bishop, Knight, Rook and Pawns.

For this exercice, you will only play with Pawns, Bishops, Rooks and Queen...
and obviously a King.

Each piece have a specific method of movement, and all patterns of capture are
detailled in the examples.txt file.

A piece can capture only the first ennemy piece it founds on its capture
patterns.

The board have a variable size but will remains a square. There's only one King
and all other pieces are against it. 

All other characters except those used for
pieces are considered as empty squares.

The King is considered as in a check position when an other enemy piece can
capture it. When it's the case, you will print "Success" on the standard output
followed by a newline, otherwise you will print "Fail" followed by a newline.

If there is no arguments, the program will only print a newline.

Examples:

    $> ./chessmate '..' '.K' | cat -e
    Fail$

    $> ./check_mate 'R...' '.K..' '..P.' '....' | cat -e
    Success$

    $> ./chessmate 'R...' 'iheK' '....' 'jeiR' | cat -e
    Success$

    $> ./chessmate | cat -e
    $

    $>

--------------------------------------------------------------------------------

From "examples.txt":

Pion / Pawn (P):

	. . . . . . .
	. . . . . . .
	. . X . X . .
	. . . P . . .
	. . . . . . .
	. . . . . . .
	. . . . . . .

Fou / Bishop (B):

	X . . . . . X
	. X . . . X .
	. . X . X . .
	. . . B . . .
	. . X . X . .
	. X . . . X .
	X . . . . . X

Tour / Rook (R):

	. . . X . . .
	. . . X . . .
	. . . X . . .
	X X X R X X X
	. . . X . . .
	. . . X . . .
	. . . X . . .

Dame / Queen (Q)

	X . . X . . X
	. X . X . X .
	. . X X X . .
	X X X Q X X X
	. . X X X . .
	. X . X . X .
	X . . X . . X 

==============================================================================

# HOW TO QUICK-COMPILE AND TEST/USE BINARY FILE

==============================================================================

1. In Windows, just click "make.bat" or type in terminal:

    $ make.bat

2. In Linux, type in terminal:

    $ make

After that, an executable (binary) is created in folder 'bin/'.
Change to folder 'bin/':

    $ cd bin

Test with following example:

    $ ./checkmate '..' '.K' | cat -e
    $ Fail

In order to debug (print chess board), insert additional option '-d':

    $ ./checkmate '..' '.K' -d
    $ Fail
    $  0 0
    $  0 K

==============================================================================

# HOW TO CROSS-COMPILE (GENERATE binary and/or .exe file) FOR LINUX AND WINDOWS

==============================================================================

I don't only compile source immediately in Windows, but also cross-compile 
in Linux (Ubuntu).

For that purpose, I installed the package Mingw-w64 (Linux):

    $ sudo apt-get install mingw-w64

and use it to cross-compile for both Linux and Windows. 

After installation of Mingw-w64, one can just type "make" or "make build" to 
build the program (compile & link):

    $ make

or

    $ make build

In the Makefile, all options for cross-compiling are made. It creates two files:

    a) "checkmate" for Linux, and

    b) "checkmate.exe" for Windows

Both files are in the subfolder "bin/" within the checkmate working directory.

All source files are found in the "src/" subfolder within the checkmate directory.

One can type "make clean" to remove the (old) binaries and files, in order to

prepare for new build process.

        $ make clean

==============================================================================

# ADDITIONAL INFORMATION

==============================================================================

INSTALL MINGW-W64 IN LINUX

1. Install Mingw-w64 (Linux):

    $ sudo apt-get install mingw-w64

2. Compile in Linux using GCC:

    $ /usr/bin/gcc

 3. Cross-compile for Windows using Mingw-w64:
 
    3.1. For 32-Bit: 

        $ /usr/bin/i686-w64-mingw32-gcc <progname.c> -o <progname>

    3.2. For 64-Bit:

        $ /usr/bin/x86_64-w64-mingw32-gcc <progname.c> -o <progname>
		
==============================================================================

INSTALL MINGW-W64 IN WINDOWS

1. Go to http://mingw-w64.org/doku.php/download

2. Download MinGW-W64 for Windows OS

3. Set the path to "bin/" folder of the installed MinGW-W64 as global.
   (In "bin/" are the files "i686-w64-mingw32-gcc.exe" and "x86_64-w64-mingw32-gcc")
   
4. Compile/Build the executable for x86 and 64bit version Windows:

	4.1. For 32-Bit: 

        $ i686-w64-mingw32-gcc <progname.c> -o <progname>

    4.2. For 64-Bit:

        $ x86_64-w64-mingw32-gcc <progname.c> -o <progname> 
